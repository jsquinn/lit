// Lists Implementing Turning
// Copyright Jonathon Quinn

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// interactive usage: lit [:rulesfile.lit] [:morefiles...] -
// commandline usage: lit [:rulesfile.lit] <expression>

// LIT (Lists Implementing Turing completeness) is yet another lame pure functional declarative language
// inspired by lisp/scheme/haskell but with list trees as the only type.
// This was an amateur experiment at implementing a fairly minimal, highly inefficient
// turing complete language that is much more usable than a turing machine
// and for testing the zig development experience.
//
// Lists are the only type and can only contain variable numbers of lists but with
// copious amounts of imagination can represent any data type (numbers, json videos)
// and perform any computation (slowly...if stack/memory don't run out).
// Grotesque example:
//  A list of 'x' empty lists can represent the positive integer 'x'
//  an ascii/utf character can be presented by an 'integer'
//  a character string can be represented by a list of 'characters'
//  a struct can be presented by a list of 'characters', 'numbers', 'strings'
//  nodes of a json/xml tree could be represented by 2-3 entry lists
//    a 'integer' node type, node type specific 'struct' and child nodes
//  ... no I haven't bothered to try this.

// Simple lists can be specified in lit expressions by positive numbers
//  '0' = empty list
//  '1'... = list containing that number of empty lists
// More complicated list trees can be built with the inbuilt 'pp <x> <y> rule
//  which returns a new list with x prepended to y
//  'pp 0 0' equals '1', 'pp 0 1' equals '2'
//  'pp 1 1' does not equal 2 or, its a list with two sublists
//     the first with one empty sub list the other empty
//  e.g. pp (()) {[][]} => {(())[][]}     mixed parentheses to visualise relation
//  Numbers beyond 0 can actually be represented with 'pp' and '0'
//    so break the minimal rule but are convenient.
//
// expression syntax:
//   <number>
//   <Named parameter>
//   <Named non-emtpy parameter>@
//   <Named non-empty parameter>#
//   <rule> <parameter>...
// parameters are expressions where rule invocations are in parenthesis
//  to avoid ambiguity of which rule a parameter is for.
// 'if condition ifnonempty ifempty' is the only other inbuilt rule
//  although it is unnecessary it is an optimisation to avoid
//  both 'ifnonempty' and 'ifempty' being calculated.
//   e.g. 'if 5 (pp 0 2) 0' will return '3'

// Rules just specify expression substitution with haskel like pattern matching
//  of parameters so barely deserve to be called functions.
//  <rule name> <parameters>... : <expression>
//  - Number parameters match an exact list shape (rarely anything but 0).
//  - Named parameters match any list and can be used in the following expression
//  - Named parameters suffixed with a '+' match a non-empty list allowing it to be dissected
//      with Name@ referring to the head list, Name# the tail, Name the complete list
//  - Names are [A-Za-z_][A-Za-z_0-9]*
//  Rules will only match with the correct number of parameters hence () is used
//    to avoid ambiguity in the expressions with multiple rule invocations
//  e.g 'myrule 0 : 0'
//      'myrule a+ : pp a@ (myrule a#)'
//      myrule will return a list with  the 1st, 3rd, 5th, 7th... elements
//      that are present in the input
//
// Rules must be passed in rule files, they cannot be not part of an expression.
// Somewhat like the difference between the initial value on the turing tape
// and the state logic of the machine.
//
// interactive usage: lit [:rulesfile.lit] [:morefiles...] -
// commandline usage: lit [:rulesfile.lit] [:morefiles...] <expression>
//
// Output uses '[]' for the outer most and non-empty lists and '*' for empty lists
// For your convenience this is not a valid lit expression.
//

//todo sort out memory allocation
//todo consider stack usage when recursing
//todo clean up code (fn/type names, namepaces)
//todo more testing with complicated lists

const std = @import("std");
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;

fn out(comptime format: []const u8, args: anytype) void {
    std.io.getStdOut().writer().print(format, args) catch unreachable;
}

const LitStream = struct {
    src: []u8,
    pos: usize = 0,

    const Self = @This();
    fn end(s: *const Self) bool {
        return s.pos >= s.src.len;
    }
    fn read(s: *const Self) u8 {
        return s.src[s.pos];
    }
    fn next(s: *Self) void {
        s.pos += 1;
    }
};

const LitParameterLimit: usize = 256;
const LitSubExpressionLimit: usize = 1024;
const LitValueEntryLimit: usize = 1024 * 1024;

const LitSymbol = struct {
    const RuleSeparator = ':';
    const ParamNotEmpty = '+';
    const ParamHead = '@';
    const ParamTail = '#';
    const StartParenthesis = '(';
    const EndParenthesis = ')';
    const Comment = '#';
};

const LitParseError = error{
    UnexpectedEOF,
    UnexpectedEOL,
    UnexpectedChar,
    ExpectedSubExpression,
    MissingParenthesis,
    NotEmptyParamRequired,
    ParameterLimitReached,
    RedefiningBuiltinRule,
};

const LitParamTag = enum {
    NumLiteral,
    Var,
    VarNotEmpty,
};

const LitParam = union(LitParamTag) {
    NumLiteral: struct { value: usize, start: usize },
    Var: struct { name: []u8, start: usize },
    VarNotEmpty: struct { name: []u8, start: usize },
};

const LitExpressionTag = enum {
    NumLiteral,
    Parameter,
    ParamHead,
    ParamTail,
    RuleInvocation,
};

const LitExpression = union(LitExpressionTag) {
    NumLiteral: usize,
    Parameter: usize,
    ParamHead: usize,
    ParamTail: usize,
    RuleInvocation: RuleInvocation,

    const RuleInvocation = struct {
        name: []u8, paramExpressions: []usize
    };
    const Self = @This();
    fn free(s: *const Self, alloc: *Allocator) void {
        if (s.* == .RuleInvocation) {
            alloc.free(s.RuleInvocation.paramExpressions);
        }
    }
};

// Expression tree stores an expression as a acyclic graph of nodes.
// For rule invocation's subexpressions are referenced by indices in the tree.
// rootExpression is the index of the top level rule invocation.
const LitExpressionTree = struct {
    expressions: []LitExpression,
    rootExpression: usize,

    const Self = @This();
    fn free(tree: *const Self, alloc: *Allocator) void {
        for (tree.expressions) |x|
            x.free(alloc);
        alloc.free(tree.expressions);
    }
};

// Expression cache is for temporary storing of expression nodes
// until the final expression size is known to store in a tree.
const LitExpressionCache = struct {
    expressions: [LitSubExpressionLimit]LitExpression = undefined,
    nExpressions: usize = 0,

    const Self = @This();
    fn add(cache: *Self, exp: LitExpression) usize {
        assert(cache.nExpressions < cache.expressions.len);
        cache.expressions[cache.nExpressions] = exp;
        cache.nExpressions += 1;
        return cache.nExpressions - 1;
    }

    fn free(cache: *const Self, alloc: *Allocator) void {
        var i = cache.nExpressions;
        while (i > 0) {
            i -= 1;
            cache.expressions[i].free(alloc);
        }
    }

    fn move(cache: *Self, alloc: *Allocator) ![]LitExpression {
        var exprs = try std.mem.dupe(alloc, LitExpression, cache.expressions[0..cache.nExpressions]);
        errdefer alloc.free(exprs);
        cache.nExpressions = 0;
        return exprs;
    }
};

// Rule maps a Name and parameters to an expression
const LitRule = struct {
    name: []u8,
    params: []LitParam,
    expression: LitExpressionTree,

    const Self = @This();
    fn free(s: *const Self, alloc: *Allocator) void {
        alloc.free(name);
        alloc.free(s.params);
        s.expression.free(alloc);
    }
};

fn isWhitespace(c: u8) bool {
    return switch (c) {
        ' ', '\t', '\r' => true,
        else => false,
    };
}

fn isEOL(c: u8) bool {
    return c == '\n';
}

fn skipWhitespace(s: *LitStream) bool {
    while (!s.end() and isWhitespace(s.read())) {
        s.next();
    }
    return !s.end();
}

fn skipWhitespaceToEndLine(s: *LitStream) !void {
    if (skipWhitespace(s) and !isEOL(s.read()))
        return error.UnexpectedChar;
}

fn skipWhitespaceToNext(s: *LitStream) !void {
    if (!skipWhitespace(s))
        return error.UnexpectedEOF;

    if (isEOL(s.read()))
        return error.UnexpectedEOL;
}

fn isNameStartChar(c: u8) bool {
    return switch (c) {
        'a'...'z' => true,
        'A'...'Z' => true,
        '_' => true,
        else => false,
    };
}

fn isNameChar(c: u8) bool {
    return switch (c) {
        'a'...'z' => true,
        'A'...'Z' => true,
        '0'...'9' => true,
        '_' => true,
        else => false,
    };
}

fn isNumChar(c: u8) bool {
    return switch (c) {
        '0'...'9' => true,
        else => false,
    };
}

fn readName(s: *LitStream) ![]u8 {
    if (!isNameStartChar(s.read()))
        return error.UnexpectedChar;

    const start = s.pos;
    s.next();

    var len: usize = 1;
    while (!s.end() and isNameChar(s.read())) {
        len += 1;
        s.next();
    }

    return s.src[start..(start + len)];
}

fn readNum(s: *LitStream) !usize {
    if (!isNumChar(s.read()))
        return error.UnexpectedChar;

    var n: usize = 0;
    while (!s.end() and isNumChar(s.read())) {
        n = n * 10 + (s.read() - '0');
        s.next();
    }

    return n;
}

fn readParam(s: *LitStream) !LitParam {
    const start = s.pos;
    if (isNumChar(s.read())) {
        return LitParam{
            .NumLiteral = .{ .value = try readNum(s), .start = start },
        };
    } else if (isNameChar(s.read())) {
        var name = try readName(s);
        if (!s.end() and s.read() == LitSymbol.ParamNotEmpty) {
            s.next();
            return LitParam{ .VarNotEmpty = .{ .name = name, .start = start } };
        } else
            return LitParam{ .Var = .{ .name = name, .start = start } };
    }

    return error.UnexpectedChar;
}

fn readParams(s: *LitStream, alloc: *Allocator) ![]LitParam {
    var nparams: usize = 0;
    var paramsTmp: [LitParameterLimit]LitParam = undefined;
    while (true) {
        try skipWhitespaceToNext(s);

        if (s.read() == LitSymbol.RuleSeparator) {
            s.next();
            return try std.mem.dupe(alloc, LitParam, paramsTmp[0..nparams]);
        } else {
            assert(nparams < paramsTmp.len);
            paramsTmp[nparams] = try readParam(s);
            nparams += 1;
        }
    }
}

/// readExpression : reads one of
/// ....Rule [<subexpression>]*     (when !isSub to avoid ambiguity with Rule Rule <value> )
/// ...<number>
/// ...<parameter>
/// ...(<expression>)
fn readExpression(s: *LitStream, isSub: bool, params: []const LitParam, cache: *LitExpressionCache, alloc: *Allocator) anyerror!usize {
    try skipWhitespaceToNext(s);

    if (s.read() == LitSymbol.StartParenthesis) {
        s.next();
        var sub = try readExpression(s, false, params, cache, alloc);
        try skipWhitespaceToNext(s);

        if (s.read() != LitSymbol.EndParenthesis)
            return error.MissingParenthesis;

        s.next();
        return sub;
    }

    if (isNumChar(s.read())) {
        var num = try readNum(s);
        return cache.add(LitExpression{
            .NumLiteral = num,
        });
    }

    if (!isNameStartChar(s.read()))
    //note could be EndParenthesis should be handled by caller...
        return error.UnexpectedChar;

    var name = try readName(s);

    var maybeParamIndex: ?usize = null;
    for (params) |p, i| {
        switch (p) {
            LitParamTag.NumLiteral => continue,
            LitParamTag.Var => |v| if (!std.mem.eql(u8, v.name, name)) continue,
            LitParamTag.VarNotEmpty => |v| if (!std.mem.eql(u8, v.name, name)) continue,
        }

        maybeParamIndex = i;
        break;
    }

    if (maybeParamIndex) |paramIndex| {
        if (!s.end() and s.read() == LitSymbol.ParamTail) {
            if (params[paramIndex] != .VarNotEmpty)
                return error.NotEmptyParamRequired;
            s.next();
            return cache.add(LitExpression{ .ParamTail = paramIndex });
        }

        if (!s.end() and s.read() == LitSymbol.ParamHead) {
            if (params[paramIndex] != .VarNotEmpty)
                return error.NotEmptyParamRequired;
            s.next();
            return cache.add(LitExpression{ .ParamHead = paramIndex });
        }

        return cache.add(LitExpression{ .Parameter = paramIndex });
    }

    // not an parameter, maybe a rule invocation
    // not allowed on a subexpression (parenthesis required)
    if (isSub)
        return error.ExpectedSubExpression;

    // better be a rule invocation
    var paramExpr: [LitParameterLimit]usize = undefined;
    var nParamExpr: usize = 0;
    ParamExpLoop: while (nParamExpr < LitParameterLimit) {
        if (readExpression(s, true, params, cache, alloc)) |expr| {
            paramExpr[nParamExpr] = expr;
            nParamExpr += 1;
        } else |err| switch (err) {
            error.UnexpectedEOF, error.UnexpectedEOL, error.UnexpectedChar => break :ParamExpLoop,
            else => return err,
        }
    }

    var paramExprs = try std.mem.dupe(alloc, usize, paramExpr[0..nParamExpr]);
    errdefer alloc.free(paramExprs);

    return cache.add(LitExpression{
        .RuleInvocation = .{ .name = name, .paramExpressions = paramExprs },
    });
}

fn readExpressionTree(s: *LitStream, params: []const LitParam, alloc: *Allocator) !LitExpressionTree {
    var cache = LitExpressionCache{};
    errdefer cache.free(alloc);
    var rootExpr = try readExpression(s, false, params, &cache, alloc);
    return LitExpressionTree{
        .expressions = try cache.move(alloc),
        .rootExpression = rootExpr,
    };
}

/// readRule Reads:
///..<name>..<params..:>..<expression>..\n>
/// ignores empty lines or those starting with #
fn readRule(s: *LitStream, alloc: *Allocator) !?LitRule {
    if (!skipWhitespace(s))
        return null;

    if (isEOL(s.read())) {
        s.next();
        return null;
    }

    if (s.read() == LitSymbol.Comment) {
        while (!s.end()) {
            if (isEOL(s.read())) {
                s.next();
                return null;
            }
            s.next();
        }
        return null;
    }

    var name = try readName(s);

    var params = try readParams(s, alloc);
    errdefer alloc.free(params);

    var expression = try readExpressionTree(s, params, alloc);
    errdefer expression.free(alloc);

    try skipWhitespaceToEndLine(s);

    return LitRule{
        .name = try std.mem.dupe(alloc, u8, name),
        .params = params,
        .expression = expression,
    };
}

fn displayExpression(exprTree: LitExpressionTree, index: usize) void {
    const ex: *const LitExpression = &exprTree.expressions[index];
    switch (ex.*) {
        .NumLiteral => |num| out("{}", .{num}),
        .Parameter => |pIndex| out("${}", .{pIndex}),
        .ParamHead => |pIndex| out("${}{c}", .{ pIndex, LitSymbol.ParamHead }),
        .ParamTail => |pIndex| out("${}{c}", .{ pIndex, LitSymbol.ParamTail }),
        .RuleInvocation => |rule| {
            out("{c}{} ", .{ LitSymbol.StartParenthesis, rule.name });
            for (rule.paramExpressions) |exprIndex, i| {
                if (i > 0)
                    out(" ", .{});
                displayExpression(exprTree, exprIndex);
            }
            out("{c}", .{LitSymbol.EndParenthesis});
        },
    }
}

fn displayRule(rule: LitRule) void {
    out("rule {}\n", .{rule.name});

    out("-> ", .{});
    for (rule.params) |param| {
        switch (param) {
            LitParamTag.NumLiteral => |p| out("{} ", .{p.value}),
            LitParamTag.Var => |p| out("{} ", .{p.name}),
            LitParamTag.VarNotEmpty => |p| out("{}{c} ", .{ p.name, LitSymbol.ParamNotEmpty }),
        }
    }
    out("{c} ", .{LitSymbol.RuleSeparator});
    displayExpression(rule.expression, rule.expression.rootExpression);
    out("\n", .{});
}

// a list is a LitValueRef which may refer to a
// LitValueEntry tree in the LitValueTable.
// Kinda like a pointer to a cons tree but optimised
// in the Ref and Entries for leading and following lengths of
// empty lists.
// A LitValueEntry is only required for each non-empty internal list
// and since is immutable but can have multiple references we use
// reference counting to clean up orphaned Entries while computing
// an expression.

const LitValueRef = struct {
    num: usize,
    tail: usize,
};

const LitValueEntry = struct {
    head: LitValueRef,
    tail: LitValueRef,
    refCount: usize,
};

const LitValueTable = struct {
    entries: [LitValueEntryLimit]LitValueEntry = undefined,
    nEntries: usize,
    holes: [LitValueEntryLimit]usize = undefined,
    nHoles: usize,
    nRefs: usize,
    logDebug: bool,

    const Self = @This();

    fn getHole(table: *Self) usize {
        if (table.nHoles > 0) {
            table.nHoles -= 1;
            var hole = table.holes[table.nHoles];
            if (table.logDebug) out("fillhole {} ({})\n", .{ hole, table.nHoles });
            return hole;
        } else {
            table.nEntries += 1;
            return table.nEntries - 1;
        }
    }

    fn addEntry(table: *Self, head: LitValueRef, tail: LitValueRef) LitValueRef {
        assert(table.nEntries > 0);
        assert(table.nEntries < table.entries.len);
        assert(head.num != 0 or head.tail != 0); // shouldn't have an empty head

        var hole = table.getHole();
        table.entries[hole] = .{
            .head = table.dupref(head),
            .tail = table.dupref(tail),
            .refCount = 1,
        };
        if (table.logDebug) out("newref {} ({})\n", .{ hole, table.entries[hole].refCount });
        return LitValueRef{ .num = 0, .tail = hole };
    }

    fn addref(table: *Self, num: usize, tail: usize) LitValueRef {
        assert(table.nEntries > 0);
        assert(tail < table.entries.len);
        assert(table.entries[tail].refCount > 0);

        if (tail > 0) {
            table.entries[tail].refCount += 1;
            if (table.logDebug) out("addref {} ({})\n", .{ tail, table.entries[tail].refCount });
            table.nRefs += 1;
        }
        return .{ .num = num, .tail = tail };
    }

    fn dupref(table: *Self, ref: LitValueRef) LitValueRef {
        return addref(table, ref.num, ref.tail);
    }

    fn deref(table: *Self, ref: LitValueRef) void {
        assert(table.nEntries > 0);
        assert(ref.tail < table.entries.len);
        assert(table.entries[ref.tail].refCount > 0);

        if (ref.tail > 0) {
            table.entries[ref.tail].refCount -= 1;
            if (table.logDebug) out("deref {} ({})\n", .{ ref.tail, table.entries[ref.tail].refCount });

            if (table.entries[ref.tail].refCount == 0) {
                table.deref(table.entries[ref.tail].head);
                table.deref(table.entries[ref.tail].tail);
                table.holes[table.nHoles] = ref.tail;
                table.nHoles += 1;
                if (table.logDebug) out("hole {} ({})\n", .{ ref.tail, table.nHoles - 1 });
            }
        }
    }

    fn getHead(table: *Self, ref: LitValueRef) LitValueRef {
        assert(ref.num != 0 or ref.tail != 0);
        if (ref.num > 0)
            return table.addref(0, 0);

        assert(ref.tail < table.entries.len);
        var value = table.entries[ref.tail];
        assert(value.refCount > 0);
        return table.dupref(value.head);
    }

    fn getTail(table: *Self, ref: LitValueRef) LitValueRef {
        assert(ref.num != 0 or ref.tail != 0);
        if (ref.num > 0)
            return table.addref(ref.num - 1, ref.tail);

        assert(ref.tail < table.entries.len);
        var value = table.entries[ref.tail];
        assert(value.refCount > 0);
        return table.dupref(value.tail);
    }

    fn init(table: *Self, logDebug: bool) void {
        // add a zero entry because that 0 is an invalid reference
        table.entries[0] = .{
            .head = .{ .num = 0, .tail = 0 },
            .tail = .{ .num = 0, .tail = 0 },
            .refCount = 1,
        };
        table.nEntries = 1;
        table.nHoles = 0;
        table.nRefs = 0;
        table.logDebug = logDebug;
    }
};

const LitComputeError = error{
    UnrecognisedRule,
    IncorrectNumParameters,
};

const LitBuildinRules = [_][]const u8{ "pp", "if" };

fn printInnerValue(values: LitValueTable, ref: LitValueRef) void {
    var num = ref.num;
    while (num > 0) {
        out("*", .{});
        num -= 1;
    }

    if (ref.tail != 0) {
        out("[", .{});
        assert(ref.tail < values.nEntries);
        printInnerValue(values, values.entries[ref.tail].head);
        out("]", .{});
        printInnerValue(values, values.entries[ref.tail].tail);
    }
}

fn printValue(values: LitValueTable, ref: LitValueRef) void {
    out("[", .{});
    printInnerValue(values, ref);
    out("]", .{});
}

const LitRuleSet = struct {
    const Map = std.StringHashMap(std.SegmentedList(LitRule, 0));
    ruleSet: Map = undefined,

    const Self = @This();
    fn init(alloc: *Allocator) Self {
        var rs = Map.init(alloc);
        return Self{
            .ruleSet = rs,
        };
    }

    fn deinit(rs: *Self) void {
        var it = rs.ruleSet.iterator();
        while (it.next()) |entry| {
            entry.value.deinit();
        }
        rs.ruleSet.deinit();
    }

    fn addRule(rs: *Self, lr: LitRule) !void {
        // todo validation
        for (LitBuildinRules) |n|
            if (std.mem.eql(u8, lr.name, n))
                return error.RedefiningBuiltinRule;

        var listGetResult = try rs.ruleSet.getOrPut(lr.name);
        if (!listGetResult.found_existing)
            listGetResult.entry.value = std.SegmentedList(LitRule, 0).init(rs.ruleSet.allocator);
        try listGetResult.entry.value.push(lr);
    }

    fn findRule(rs: Self, ruleName: []u8, params: []LitValueRef) ?*const LitRule {
        if (rs.ruleSet.get(ruleName)) |ruleList| {
            var it = ruleList.iterator(0);
            nextRule: while (it.next()) |rule| {
                if (rule.params.len != params.len)
                    continue :nextRule;

                nextParam: for (rule.params) |p, i| {
                    switch (p) {
                        LitParam.NumLiteral => |nl| if (params[i].tail == 0 and params[i].num == nl.value)
                            continue :nextParam
                        else
                            continue :nextRule,
                        LitParam.Var => continue :nextParam,
                        LitParam.VarNotEmpty => |vne| if (params[i].num != 0 or params[i].tail != 0)
                            continue :nextParam
                        else
                            continue :nextRule,
                    }
                }

                return rule;
            }
        }

        return null;
    }
};

fn parseRules(src: []u8, rules: *LitRuleSet, alloc: *Allocator) !void {
    var s = LitStream{
        .src = src,
    };
    while (!s.end()) {
        var optRuleOrErr = readRule(&s, alloc);

        if (optRuleOrErr) |maybeOptRule| {
            if (maybeOptRule) |rule| {
                try rules.addRule(rule);
            }
        } else |err| {
            var c: u8 = if (s.pos < src.len) src[s.pos] else '?';
            var pos: usize = s.pos;
            if (pos > 0 and src[pos - 1] == '\r')
                pos -= 1;
            out("{}<===char {}@{}\n", .{ src[0..pos], c, pos });
            return err;
        }
    }
}

const LitComputeContext = struct {
    expr: LitExpressionTree,
    params: []const LitValueRef,
    values: *LitValueTable,
    rules: LitRuleSet,
    alloc: *Allocator,
    logDebug: bool,
};

fn rulePp(ruleInv: LitExpression.RuleInvocation, context: LitComputeContext) anyerror!LitValueRef {
    if (ruleInv.paramExpressions.len != 2)
        return LitComputeError.IncorrectNumParameters;

    var left = try compute(context, ruleInv.paramExpressions[0]);
    var right = try compute(context, ruleInv.paramExpressions[1]);
    defer context.values.deref(left);
    defer context.values.deref(right);

    if (left.num == 0 and left.tail == 0)
        return context.values.addref(right.num + 1, right.tail);

    return context.values.addEntry(left, right);
}

fn ruleIf(ruleInv: LitExpression.RuleInvocation, context: LitComputeContext) anyerror!LitValueRef {
    if (ruleInv.paramExpressions.len != 3)
        return LitComputeError.IncorrectNumParameters;

    var cond = try compute(context, ruleInv.paramExpressions[0]);
    defer context.values.deref(cond);

    if (cond.num == 0 and cond.tail == 0)
        return try compute(context, ruleInv.paramExpressions[2]);
    return try compute(context, ruleInv.paramExpressions[1]);
}

fn printRuleInvocation(rule: LitRule, params: []const LitValueRef, values: LitValueTable) void {
    out("calling ", .{});
    displayRule(rule);

    if (params.len > 0) {
        out("with ", .{});
        for (params) |ip, ipn| {
            if (ipn > 0) out(", ", .{});
            printValue(values, ip);
        }
        out("\n", .{});
    }
}

fn compute(context: LitComputeContext, exprIndex: usize) anyerror!LitValueRef {
    assert(exprIndex < context.expr.expressions.len);
    switch (context.expr.expressions[exprIndex]) {
        .NumLiteral => |num| return LitValueRef{ .num = num, .tail = 0 },
        .Parameter => |pindex| {
            assert(pindex < context.params.len);
            return context.values.dupref(context.params[pindex]);
        },
        .ParamHead => |pindex| {
            assert(pindex < context.params.len);
            return context.values.getHead(context.params[pindex]);
        },
        .ParamTail => |pindex| {
            assert(pindex < context.params.len);
            return context.values.getTail(context.params[pindex]);
        },
        .RuleInvocation => |ruleInv| {
            if (std.mem.eql(u8, ruleInv.name, "pp"))
                return try rulePp(ruleInv, context);
            if (std.mem.eql(u8, ruleInv.name, "if"))
                return try ruleIf(ruleInv, context);

            var invParams = try context.alloc.alloc(LitValueRef, ruleInv.paramExpressions.len);
            var i: usize = 0;
            // only deref invParam that were allocated...
            defer while (i > 0) : (i -= 1) {
                context.values.deref(invParams[i - 1]);
            };

            while (i < invParams.len) : (i += 1) {
                invParams[i] = try compute(context, ruleInv.paramExpressions[i]);
            }

            if (context.rules.findRule(ruleInv.name, invParams)) |rule| {
                if (context.logDebug)
                    printRuleInvocation(rule.*, invParams, context.values.*);

                var newContext = LitComputeContext{
                    .expr = rule.expression,
                    .params = invParams,
                    .values = context.values,
                    .rules = context.rules,
                    .alloc = context.alloc,
                    .logDebug = context.logDebug,
                };

                var result = try compute(newContext, rule.expression.rootExpression);
                if (context.logDebug) {
                    out("= ", .{});
                    printValue(context.values.*, result);
                    out("\n", .{});
                }
                return result;
            } else
                return LitComputeError.UnrecognisedRule;
        },
    }
}

fn run(src: []u8, logDebug: bool, rules: LitRuleSet, alloc: *Allocator) !void {
    if (logDebug) out("read expression : {}\n", .{src});

    var stream = LitStream{
        .src = src,
    };
    const params = [0]LitParam{};
    var expression = try readExpressionTree(&stream, &params, alloc);
    errdefer expression.free(alloc);

    var values = try alloc.create(LitValueTable);
    defer alloc.destroy(values);
    values.init(logDebug);
    if (logDebug) {
        out("compute expression : ", .{});
        displayExpression(expression, expression.rootExpression);
        out("\n", .{});
    }

    var newContext = LitComputeContext{
        .expr = expression,
        .params = &[_]LitValueRef{},
        .values = values,
        .rules = rules,
        .alloc = alloc,
        .logDebug = logDebug,
    };

    var value = try compute(newContext, expression.rootExpression);
    printValue(values.*, value);
    out("\n", .{});
    values.deref(value);
    if (logDebug)
        out("stats max = {}, holes = {}, refs = {}\n", .{ values.nEntries, values.nHoles, values.nRefs });
}

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    var alloc = &arena.allocator;

    const args = try std.process.argsAlloc(alloc);
    defer std.process.argsFree(alloc, args);

    var rules = LitRuleSet.init(alloc);
    defer rules.deinit();

    var useStdin = false;
    var logDebug = false;
    for (args) |arg, i| {
        if (i == 0)
            continue;

        if (arg.len > 0 and arg[0] == '-') {
            if (std.mem.eql(u8, "-", arg)) {
                var input: [1024]u8 = undefined;
                const stdin = std.io.getStdIn().inStream();
                while (true) {
                    const maybeLine = stdin.readUntilDelimiterOrEof(input[0..], '\n') catch |err| switch (err) {
                        error.StreamTooLong => {
                            out("Input too long.\n", .{});
                            continue;
                        },
                        else => return err,
                    };

                    if (maybeLine) |line| {
                        if (std.mem.eql(u8, line, "exit") or std.mem.eql(u8, line, "exit\r") or std.mem.eql(u8, line, "quit") or std.mem.eql(u8, line, "quit\r"))
                            break;

                        run(line, logDebug, rules, alloc) catch |err| {
                            out("error = {}\n", .{err});
                        };
                    }
                }
            } else if (std.mem.eql(u8, "-d", arg)) {
                logDebug = true;
            } else {
                out("lit [-d] [:rulesfile.lit] [-|expression]\n", .{});
            }
        } else if (arg.len > 0 and arg[0] == ':') {
            // rule file
            out("Reading rules: {}\n", .{arg[1..arg.len]});
            var f = try std.fs.cwd().openFile(arg[1..arg.len], std.fs.File.OpenFlags{});
            var stat = try f.stat();
            var content = try f.readAllAlloc(alloc, stat.size, std.math.maxInt(usize));
            //defer alloc.free(content); // todo first expression rule invocation names must be allocated
            try parseRules(content, &rules, alloc);
        } else {
            //expression
            try run(arg, logDebug, rules, alloc);
        }
    }
}
